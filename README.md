# README #


### What is this repository for? ###

This repository is for the storing and sharing of useful SQL scripts.

### Contribution guidelines ###

* For consistency please create the files in the following format: 


  **all-lowercase-split-by-dashes.sql**   

* Please include a comment in the script describing the nature of it

* Feel free to create sub folders to group related scripts

### Who do I talk to? ###

* Repo created by Lee Englestone, can be used by anyone